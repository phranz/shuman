########################################################################
# Copyright (C) 2025 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.	If not, see <https://www.gnu.org/licenses/>.
########################################################################
 

#url https://codeberg.org/phranz/shuman/raw/branch/master/funcs/tojson

#desc converts text lines into JSON

#help
#Usage: tojson [<object name>]
#
#Reads lines from stdin and split them into fields,
#then prints a JSON object, optionally named <object name>,
#having the first element of each line as an attribute,
#and the remaining ones grouped in a list, as the value of
#that attribute.
#
#Example:
#> ps | pivot | tojson
#help

tojson() {
	awk -v o="$1" '
		BEGIN {
			if (o) printf "{\"%s\": ", o
			printf "{"
			l=0
		} {
			if (l)
				printf ", "
			printf "\"%s\": [", $1
			for (i=2; i<=NF; ++i)
				printf "%s", (i==NF ? "\""$i"\"" : "\""$i"\", ")
			printf "]"
			l=1
		}
		END { 
			printf "}"
			if (o) printf "}"
			printf "\n"
		}
	'
}
