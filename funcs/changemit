########################################################################
# Copyright (C) 2025 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.	If not, see <https://www.gnu.org/licenses/>.
########################################################################


#url https://codeberg.org/phranz/shuman/raw/branch/master/funcs/changemit

#desc creates changelog.md from all commits of a repo by using tags

#help
#deps git
#Usage: changemit <tagver> <commit msg>
#
#(Re)Generate a changelog.md file in the current git repo by using
#available tags to organiza information.
#<tagver> and <commit msg> are respectively the last tag to create and
#the last commit message (in which the changelod.md is included)
#
#Ex.:
#cd myrepo; changemit 0.2.1 'my commit message'
#help

changemit() {
	(
		rem=$(git remote get-url origin 2> /dev/null)
		test "$rem" || {
			printf 'Not in a git repo!\n' 1<&2
			exit 1
		}
		printf '%s\n' "$rem" | grep -q '^git@' && {
			url=$(printf '%s\n' "$rem" | sed 's/git@//;s/:/\//;s/\.git$//;s#^#https://#')
			:
		} || {
			url=$(printf '%s\n' "$rem" | sed 's#//[^:@\.]*[^:@\.]*@#//#;s/\.git$//')
		}
		find | sed 's/\(.\)/\\\1/g' | xargs touch
		exec 3<&1
		exec 1>changelog.md
		tdate=$(date '+%Y-%m-%d')
		printf '%s\n\n' "## $1 (${tdate})"
		printf '%s\n\n' "*	$2"
		pt=0
		for ct in $(git tag --sort=-creatordate)
		do
			test "$pt" = 0 || {
				tdate="$(git log -1 --pretty=format:'%ad' --date=short ${pt})"
				printf '%s\n\n' "## ${pt} (${tdate})"
				git log "${ct}...${pt}" --pretty=format:"*	%s [commit](${url}/commit/%H)" --reverse | grep -v Merge
				printf "\n\n"
			}
			pt="${ct}"
		done
		exec 1<&3
		git status
		git add . && git commit -m "$2"
		git tag "$1"
	)
}
