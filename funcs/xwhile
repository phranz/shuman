########################################################################
# Copyright (C) 2025 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.	If not, see <https://www.gnu.org/licenses/>.
########################################################################


#url https://codeberg.org/phranz/shuman/raw/branch/master/funcs/xwhile

#desc does things when a region of a X window changes

#help
#deps import identify compare
#Usage: xwhile [-<number of regions>][-and][-or][-eq][-neq][-win <id>][-rects <geom[,geom[...]]>] [<commands>]
#
#Using imagemagick tools, activates mouse selection to select a rectangular region 
#of a X11 window to monitor, detecting possible changes.
#
#By default the function executes <commands> until the region remains the same,
#otherwise terminates; if <commands> aren't specified, the function tries
#to read them from stdin.
#
#If <number of regions> is specified, selects and monitors a number of regions
#equivalent to <number of regions>.
#
#Options: 
#	-eq							executes commands when regions are equal.
#	-neq						executes commands when regions are not equal.
#	-and						when multiple regions are monitored, executes commands 
#								only when all conditions (depends on -eq -neq) evaluates to true.
#	-or							when multiple regions are monitored, executes commands 
#								only when at least a condition (depends on -eq -neq) evaluates to true.
#	-win <id>					specify X11 window id, by default root window is set.
#	-rects <geom[,geom[...]]>	specify rectangular regions by geometries (comma separated 
#								list of <geom> items like '10x10+2+20') instead of selecting
#								them with the mouse.
#
#Ex.
#> xwhile -2 -neq echo not equal
#
#This selects two region on the root window and executes the command
#'echo not equal' until both regions are not equal.
#help

xwhile() {
	(
		and=1
		eq=1
		conds=
		rects=
		nrects=0
		win=root
		while printf '%s\n' "$1" | grep -q '^-'
		do
			case "$1" in
				(-eq) eq=1 ;;
				(-neq) eq=0 ;;
				(-and) and=1 ;;
				(-or) and=0 ;;
				(-win) {
					shift
					printf '%s\n' "$1" | grep -q '^[0-9]\{1,\}$' || return 1
					win=$(printf '%s\n' "$1")
				} ;;
				(-rects) {
					shift
					printf '%s\n' "$1" | grep -q '^\([0-9]\{1,\}x[0-9]\{1,\}+[0-9]\{1,\}+[0-9]\{1,\},\{,1\}\)*$' || return 1
					rects=$(printf '%s\n' "$1" | tr ',' ' ')
					nrects=$(printf '%s\n' "$rects" | wc -w)
				} ;;
				(*) conds=$(printf '%s\n' "$1" | sed -n '/^-[0-9]\{1,\}$/s/^-\([0-9]\{1,\}\)$/\1/p')
			esac
			shift
		done
		conds="${conds:-1}"
		gen_volatile_png="mktemp /tmp/tmp.XXXXXXXXXXXXXX.png"
		for i in $(seq 1 "$nrects")
		do eval given_rect"$i"='"$(printf '\''%s\n'\'' "$rects" | awk '\''{ print $'"$i"'}'\'')"'
		done
		test "$nrects" -gt "$conds" && conds="$nrects"
		for i in $(seq 1 "$conds")
		do
			eval ref"$i"'="$($gen_volatile_png)"'
			eval new"$i"'="$($gen_volatile_png)"'
			eval 'test -z "${given_rect'"$i"'}"' && {
				eval probe"$i"'="$($gen_volatile_png)"'
				eval 'import "${probe'"${i}"'}"' || return 1
				eval data"$i"'="$(identify "${probe'"$i"'}")"' || return 1
				eval rect"$i"'="$(printf '\''%s\n'\'' "${data'"$i"'}" | awk '\''{ print $3 }'\'')"'
				eval pos"$i"'="$(printf '\''%s\n'\'' "${data'"$i"'}" | awk '\''{
					sub(/[0-9]+x[0-9]+\+/, "", $4)
					print "+"$4 
				}'\'')"'
			} || {
				eval rect"$i"='"${given_rect'"$i"'}"'
				eval pos"$i"=
			}
			eval 'import -window "$win" -crop "${rect'"$i"'}${pos'"$i"'}" +repage "${ref'"$i"'}"' || return 1
		done
		test "$1" || set -- $(cat)
		while : 
		do
			out=
			for i in $(seq 1 "$conds")
			do
				eval 'import -window "$win" -crop "${rect'"$i"'}${pos'"$i"'}" +repage "${new'"$i"'}"' || return 1
				eval 'compare -metric AE "${ref'"$i"'}" "${new'"$i"'}" /dev/null >/dev/null 2>/dev/null'
				out="${out}$?"
			done
			with0="$(printf '%s\n' "$out" | grep '0')"
			with1="$(printf '%s\n' "$out" | grep '1')"
			test "$eq" = 1 && {
				test "$and" = 1 && {
					test "$with1" && break || "$@"
					true
				} || {
					test "$with0" && {
						"$@"
						true
					} || break
				}
			} || {
				test "$and" = 1 && {
					test "$with0" && break || "$@"
					true
				} || {
					test "$with1" && {
						"$@"
						true
					} || break
				}
			}
		done
		for i in $(seq 1 "$conds")
		do
			eval 'test -f "${probe'"$i"'}" && unlink "${probe'"$i"'}"'
			eval 'test -f "${ref'"$i"'}" && unlink "${ref'"$i"'}"'
			eval 'test -f "${new'"$i"'}" && unlink "${new'"$i"'}"'
		done
	)
}
