########################################################################
# Copyright (C) 2025 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.	If not, see <https://www.gnu.org/licenses/>.
########################################################################


#url https://codeberg.org/phranz/shuman/raw/branch/master/funcs/left

#desc computes left joins

#help
#Usage: left <file> <col1> <operator> <col2> [ and|or <col1> <operator> <col2> [ ... ] ]
#
#Reads tabular data from stdin and <file> and computes the left join
#using <col1> from stdin table and <col2> from <file>, where both
#<col1> and <col2> are indices.
#
#Example: 
#> ls -l | get 2, | left ls_report.txt 1 == 1
#
#This computes the left join of 'ls -l' output
#and data in ls_report.txt; '1 == 1' tells the
#function to use the first column of 'ls -l' and
#the first column of ls_report.txt.
#help

left() {
	test $# -lt 4 && return 1
	(
		table="$1"
		shift
		cond=
		while test -n "$1"
		do
			field="$1"
			operator="$2"
			operand="$3"
			if test -n "$4"
			then
				case "$(printf '%s\n' "$4" | tr '[:upper:]' '[:lower:]')" in
					and) op="&&" ;; 
					or) op="||" ;;
					*) return 1 ;;
				esac
				shift 4
			else
				shift 3
				op=
			fi
			cond="$cond (\$$field $operator t[$operand]) $op"
		done
		awk -v table="$table" '
			BEGIN {
				i=1
				c=0
				while (getline row < table)
					tlines[i++]=row
			} {
				ct=0
				for (j=1 ; j<i; ++j) {
					nf=split(tlines[j], t)
					if ('"${cond}"') {
						print $0, tlines[j]
						ct=1
					}
				}
				if (!ct)
					res[c++]=$0
			} END {
				for (x=0; x<c; ++x) {
					printf "%s ", res[x]
				
					for (n=0; n<nf; ++n)
					   printf "NULL "
					printf "\n"
				}
			}
		'
	)
}
