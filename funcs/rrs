########################################################################
# Copyright (C) 2025 Francesco Palumbo <phranz.dev@gmail.com>, Naples (Italy)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.	If not, see <https://www.gnu.org/licenses/>.
########################################################################


#url https://codeberg.org/phranz/shuman/raw/branch/master/funcs/rrs

#desc runs a program with a temporary home directory, with synchronization

#help
#deps rsync
#Usage: rrs [<new home>] <executable> [<files>]
#
#Runs <executable> from a freshly created home directory (or <new home> if given), copying
#<files> whose paths are relative to HOME dir, if supplied, to new home.
#When <executable> is done,does synchronization between
#changed <files> and original ones.
#
#Ex.
#> rrs firefox .mozilla
#help

rrs() {
	(
		set -e
		test "$1" || return 1
		test -d "$1" && {
			nodel=1
			mdir="$1"
			shift
			test "$1" || return 1
		}
		prog="$1"
		shift
		test -d "$mdir" || mdir=$(mktemp -d)
		files=
		for a
		do
			test -e ~/"$a" || continue
			d=$(dirname "$a")
			if test "$d" != "."
			then
				test -d "${mdir}/$d" || mkdir -p "${mdir}/${d}"
			fi
			files="$files $a"
			cp -aT ~/"$a" "${mdir}/${a}"
		done
		oldhome="$HOME"
		export HOME="$mdir"
		old=$(pwd)
		cd
		$prog
		cd "$old"
		export HOME="$oldhome"
		for a in $files
		do
			if test -d "${mdir}/${a}"
			then
				rsync -a --delete "${mdir}/${a}/" "${HOME}/${a}"
			else
				rsync -a --delete "${mdir}/${a}" "${HOME}/${a}"
			fi
		done
		test "$nodel" = 1 || rm -rf "$mdir"
	)
}
